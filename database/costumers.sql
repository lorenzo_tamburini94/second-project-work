-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mag 27, 2020 alle 12:13
-- Versione del server: 10.4.10-MariaDB
-- Versione PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_work_2`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `costumers`
--

CREATE TABLE `costumers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `costumers`
--

INSERT INTO `costumers` (`id`, `name`, `city`, `email`, `phone`, `age`, `created_at`, `updated_at`) VALUES
(1, 'Manuela Albi', 'rimini', 'maria@gmail.com', '44454535353', 40, '2020-05-26 06:21:13', '2020-05-26 06:31:34'),
(2, 'Manuel Chiocciola', 'rimini', 'm@hotmail.com', '44454535535', 40, '2020-05-26 18:34:48', '2020-05-26 18:34:48'),
(3, 'Giovanna Bianchi', 'treviso', 'm@waiata.it', '33454533535', 45, '2020-05-26 18:39:53', '2020-05-26 18:39:53'),
(4, 'Mella Banana', 'roma', 'mella@hotmail.com', '33454535654', 28, '2020-05-26 18:40:39', '2020-05-26 18:40:39'),
(5, 'Simona Simonini', 'orvieto', 'orny@hotmail.com', '33454003654', 63, '2020-05-26 18:41:27', '2020-05-26 18:41:27'),
(6, 'Battistini Lorena', 'Palermo', 'battis@gmail.com', '3335958585', 51, '2020-05-27 07:24:28', '2020-05-27 07:26:21'),
(7, 'Zaccagni Franca', 'Mantova', 'zacca@gmail.com', '33388899977', 44, '2020-05-27 07:30:30', '2020-05-27 07:29:25'),
(8, 'Laurenti Laura', 'Livorno', 'lally@hotmail.it', '3345588999', 19, '2020-05-27 07:31:00', '2020-05-27 07:31:18'),
(9, 'Feffy Genoveffa', 'Aosta', 'feffy@gmail.it', '33266677788', 32, '2020-05-27 07:34:25', '2020-05-27 07:34:41'),
(10, 'Sanchi Eleonora', 'Rovigno', 'ely@fastweb.it', '3335577790', 45, '2020-05-27 08:11:00', '2020-05-27 08:14:19'),
(11, 'Flores Filomena', 'Genova', 'Florss@hotmail.com', '3459999055', 61, '2020-05-27 08:26:12', '2020-05-27 08:26:34'),
(12, 'Selenio Martina', 'Riva del Garda', 'riva@gmail.com', '3225566000', 30, '2020-05-27 09:42:00', '2020-05-27 09:43:00'),
(13, 'Riva Santa', 'Pordenone', 'santissima@fastweb.it', '3336655988', 39, '2020-05-27 09:49:00', '2020-05-27 09:51:00');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `costumers`
--
ALTER TABLE `costumers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `costumers`
--
ALTER TABLE `costumers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
