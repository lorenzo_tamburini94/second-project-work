-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mag 30, 2020 alle 02:56
-- Versione del server: 10.4.11-MariaDB
-- Versione PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pw2`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `costumers`
--

CREATE TABLE `costumers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `costumers`
--

INSERT INTO `costumers` (`id`, `name`, `city`, `email`, `phone`, `age`, `created_at`, `updated_at`) VALUES
(1, 'Johnny Muffa', 'Bagheria', 'jm@muffa.com', '32567466', 43, '2020-05-29 21:54:26', '2020-05-29 21:54:26'),
(2, 'Ciccio Caputo', 'Chicago', 'ccaputo@chicago.com', '32562353', 48, '2020-05-29 21:55:06', '2020-05-29 21:55:06'),
(3, 'Donnie Brasco', 'New Orleans', 'donnie@mafia.com', '3256566', 26, '2020-05-29 21:58:55', '2020-05-29 21:58:55'),
(4, 'Johnny Palomba', 'Roma', 'jp@milo.com', '4675566', 19, '2020-05-29 22:01:46', '2020-05-29 22:01:46'),
(5, 'Lello Peppo', 'Capri', 'lp@lo.com', '478566', 68, '2020-05-29 22:03:01', '2020-05-29 22:03:01'),
(6, 'Johnny Stecchino', 'Palermo', 'jost@mafia.com', '39485766', 53, '2020-05-29 22:04:35', '2020-05-29 22:04:35'),
(7, 'Gino Paoli', 'Genova', 'gp@gp.com', '3942758766', 24, '2020-05-29 22:06:03', '2020-05-29 22:06:03'),
(8, 'Pedro Rojas', 'Valencia', 'pedror@hola.es', '39475788766', 31, '2020-05-29 22:07:20', '2020-05-29 22:07:20'),
(9, 'Gildo Rojas', 'Valencia', 'gildor@hola.es', '39472288766', 44, '2020-05-29 22:08:14', '2020-05-29 22:08:14'),
(10, 'Ana Perez', 'Siviglia', 'anper@hola.es', '394344066', 42, '2020-05-29 22:09:14', '2020-05-29 22:09:14'),
(11, 'Joe Perez', 'Siviglia', 'joper@hola.es', '55344066', 46, '2020-05-29 22:10:06', '2020-05-29 22:10:06'),
(12, 'Wim Wenders', 'Berlino', 'wwen@film.com', '2983344066', 49, '2020-05-29 22:11:12', '2020-05-29 22:11:12'),
(13, 'David Lynch', 'Molfetta', 'dalyn@film.com', '2984837484', 58, '2020-05-29 22:12:18', '2020-05-29 22:12:18'),
(14, 'Richard Linklater', 'Seattle', 'rlink@film.com', '24755837484', 50, '2020-05-29 22:13:13', '2020-05-29 22:13:13'),
(15, 'Denis Villeneuve', 'Lione', 'devil@film.com', '247495207484', 44, '2020-05-29 22:14:33', '2020-05-29 22:14:33'),
(16, 'Gino Pino', 'Cagliari', 'gippo@pippo.com', '2473333484', 42, '2020-05-29 22:15:51', '2020-05-29 22:15:51'),
(17, 'Paolo Cuccureddu', 'Cagliari', 'cuccu@pippo.com', '247548484', 36, '2020-05-29 22:16:42', '2020-05-29 22:16:42'),
(18, 'Rosa Palillu', 'Cagliari', 'ropal@pippo.com', '23346484', 57, '2020-05-29 22:17:28', '2020-05-29 22:17:28'),
(19, 'Danae Boronat', 'Girona', 'dabor@daje.com', '2339484', 29, '2020-05-29 22:19:42', '2020-05-29 22:19:42'),
(20, 'Gina Azzolina', 'Rovigno', 'gazz@daje.com', '55579484', 26, '2020-05-29 22:21:22', '2020-05-29 22:21:22'),
(21, 'Paola Ramini', 'Rieti', 'pramini@daje.com', '39569484', 73, '2020-05-29 22:22:20', '2020-05-29 22:22:20'),
(22, 'Lino Panno', 'Incontinental', 'pannolino@daje.com', '127569484', 99, '2020-05-29 22:24:29', '2020-05-29 22:24:29'),
(23, 'Anna Pannocchia', 'Acitrezza', 'pannocchia@daje.com', '127584734', 54, '2020-05-29 22:25:44', '2020-05-29 22:25:44'),
(24, 'Antonella Generale', 'Ancona', 'generella@daje.com', '2384844734', 33, '2020-05-29 22:27:00', '2020-05-29 22:27:00'),
(25, 'David Reading', 'Liverpool', 'dread@daje.uk', '555844734', 69, '2020-05-29 22:29:03', '2020-05-29 22:29:03'),
(26, 'Tonia Buddy', 'Preston', 'tb@daje.uk', '5757544734', 34, '2020-05-29 22:30:22', '2020-05-29 22:30:22'),
(27, 'Winston Butterfly', 'Galway', 'winbut@daje.com', '5232234734', 46, '2020-05-29 22:32:15', '2020-05-29 22:32:15'),
(28, 'Ana Moura', 'Santarem', 'anmo@fado.pt', '5266974734', 40, '2020-05-29 22:35:45', '2020-05-29 22:35:45');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `costumers`
--
ALTER TABLE `costumers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `costumers`
--
ALTER TABLE `costumers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
