<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Costumer;
use Illuminate\Support\Facades\Validator;

class CostumersController extends Controller {

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'city' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'age' => 'required|integer'
        ]);

        if($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 400);
        }

        $costumer = new Costumer();

        $costumer->name = $request->input('name');
        $costumer->city = $request->input('city');
        $costumer->email = $request->input('email');
        $costumer->phone = $request->input('phone');
        $costumer->age = $request->input('age');
        
        try {
            $costumer->save();
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

        return $costumer;
    }
    
     public function getAll(Request $request) {
        $costumers = Costumer::paginate(15);
        return $costumers;
    }

    public function get(Request $request, $id) {
        $costumer = Costumer::findOrFail($id);
        return $costumer;
    }
}
