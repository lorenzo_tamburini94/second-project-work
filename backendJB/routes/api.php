<?php

use Illuminate\Http\Request;

Route::post('/costumers', 'CostumersController@store');
Route::get('/costumers', 'CostumersController@getAll');
Route::get('/costumers/{id}', 'CostumersController@get'); 


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
