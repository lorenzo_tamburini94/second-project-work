<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Costumers;
use Illuminate\Support\Facades\Validator;


class CostumersController extends Controller{
    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'city' => 'required',
            'email' => 'required|unique:costumers|email:rfc,dns',
            'phone' => 'required',
            'age' => 'required|integer'
            ]);
            if ($validator->fails()) {
            return response()->json([
            'messages' => $validator->errors(),
            'status' => 'error',
            ], 400);
            }

        $costumer = new Costumers();

        $costumer->name = $request->input('name');
        $costumer->city = $request->input('city');
        $costumer->email = $request->input('email');
        $costumer->phone = $request->input('phone');
        $costumer->age = $request->input('age');
        
        $costumer->save();

        return $costumer;

        
    }
    
     public function getAll(Request $request) {
        $costumers = Costumers::paginate(12);
        return $costumers;
    }

    public function get(Request $request, $id) {
        $costumer = Costumers::findOrFail($id);
        return $costumer;
    }
}
