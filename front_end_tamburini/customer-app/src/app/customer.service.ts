import { Injectable } from '@angular/core';
import { Customer, CustomerPage } from './customer.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  
  baseUrl = 'http://127.0.0.1:8000/api';

  constructor(private http: HttpClient) { }

  getBaseUrl() { 
    return this.baseUrl;
  }

  /**
   * 
   * @param id parametro passatp per cercare il singolo customers
   * @returns restituisce il singolo customers
   */
  getCustomerFromId(id: number): Promise<Customer> {
    let url = this.baseUrl + '/costumers/' + id + '/';
    return this.getCustomerFromUrl(url);
  }

  /**
   * 
   * @param url parametro passato per scaricare i costumer tramite http get
   */
  getCustomerFromUrl(url: string): Promise<Customer> {
    return this.http.get<Customer>(url).toPromise();
  }


  /**
   * 
   * @param pageUrl parametro passato, url della pagina
   */
  getCustomerPage(pageUrl: string): Promise<CustomerPage> {
    pageUrl = this.baseUrl + '/costumers/';
    return this.http.get<CustomerPage>(pageUrl).toPromise();
  }
}
