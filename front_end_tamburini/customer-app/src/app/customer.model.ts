export class Customer{
    id: number;
    name: string;
    city: string;
    email: string;
    phone: string;
    age: number;
}

export class CustomerPage{
        total: number;
        path: string;
        first_page_url: string;
        next_page_url: string;
        prev_page_url: string;
        last_page_url: string;
        data: Customer[];
}