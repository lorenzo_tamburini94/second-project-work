import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Customer, CustomerPage } from '../customer.model';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  currentCustomerPage: CustomerPage = null;  
  customerList: Customer[] = [];

  constructor(private customer: CustomerService) {}

  
  ngOnInit() {
    console.log('AppComponent.onPaginaIniziale: ');
    this.customer.getCustomerPage(null)
        .then(pagina_iniziale => {
          this.currentCustomerPage = pagina_iniziale;
          this.customerList = this.currentCustomerPage.data;
          console.log('AppComponent.getCustomerPage: ', this.currentCustomerPage);
        })
        .catch(err => {
          console.error('AppComponent.onPaginaIniziale: ', err);
        });
  }

  /**
   * 
   */
  onPaginaPrecedente() {
    console.log('AppComponent.onPaginaPrecedente: ');

    if (this.currentCustomerPage != null && this.currentCustomerPage.prev_page_url != null) {
      this.customer.getCustomerPage(this.currentCustomerPage.prev_page_url)
        .then(pagina_precedente => {
          this.currentCustomerPage = pagina_precedente;
          this.customerList = this.currentCustomerPage.data;
          console.log('AppComponent.getCharacterPage: ', this.currentCustomerPage);
        })
        .catch(err => {
          console.error('AppComponent.onPaginaPrecedente: ', err);
        });
    }
  }

  /**
   * 
   */
  onPaginaSuccessiva() {
    console.log('AppComponent.onPaginaSuccessiva: ');

    if (this.currentCustomerPage != null && this.currentCustomerPage.next_page_url != null) {
      this.customer.getCustomerPage(this.currentCustomerPage.next_page_url)
        .then(pagina_successiva => {
          this.currentCustomerPage = pagina_successiva;
          this.customerList = this.currentCustomerPage.data;
          console.log('AppComponent.getCharacterPage: ', this.currentCustomerPage);
        })
        .catch(err => {
          console.error('AppComponent.onPaginaSuccessiva: ', err);
        });
    }
  }

  /**
   * 
   */
  // onPaginaIniziale() {
  //   console.log('AppComponent.onPaginaIniziale: ');
  //   this.customer.getCustomerPage(null)
  //       .then(pagina_iniziale => {
  //         this.currentCustomerPage = pagina_iniziale;
  //         this.customerList = this.currentCustomerPage.data;
  //         console.log('AppComponent.getCustomerPage: ', this.currentCustomerPage);
  //       })
  //       .catch(err => {
  //         console.error('AppComponent.onPaginaIniziale: ', err);
  //       });
  // }

  
  /**
   * 
   */
  // onPaginaFinale() {
  //   console.log('AppComponent.onPaginaFinale: ');
  //   if (this.currentCustomerPage != null) {
      
  //     this.customer.getCustomerPage(this.currentCustomerPage.last_page_url)
  //       .then(pagina_finale => {
  //         this.currentCustomerPage = pagina_finale;
  //         this.customerList = this.currentCustomerPage.data;
  //         console.log('AppComponent.getCustomerPage: ', this.currentCustomerPage);

  //         })
  //         .catch(err => {
  //           console.error('AppComponent.onPaginaFinale: ', err);
  //         });
  //   }
  // }

  //TODO CONTROLLARE IL GET ID
  getId(customer: Customer): number {
    let result = -1;

    if (customer != null) {

      result = customer.id;
      return result;
    }

    return result;
  }

}
