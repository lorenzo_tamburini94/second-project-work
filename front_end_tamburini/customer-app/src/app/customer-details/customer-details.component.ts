import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Customer } from '../customer.model';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

  currentCustomer: Customer = null;

  constructor(private activatedRoute: ActivatedRoute,
  private customer: CustomerService) { }
  
  ngOnInit(){
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log('CustomerDetailsComponent.ngOnInit(): customer id=', id);
    if (id != null) {
      this.customer.getCustomerFromId(Number(id))
      .then(customerDetails => {
        this.currentCustomer = customerDetails;
        console.log('PageCustomerDetailsComponent.ngOnInit(): currentCustomer ',
        this.currentCustomer);
      });
    }
  }

}
